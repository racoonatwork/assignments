# Problem set 大致内容
最初problem set来源于google搜索, 但其实课程网址上就有[problem set](http://cs229.stanford.edu/ps/)。
由于完成了部分作业后，才注意到这个目录, 所以作业的时间并不一致，需要注意一下.

- ps1. **Supervised Learning.** Autumn 2016
- ps2. **Supervised Learning II.** Autumn 2017
- ps3. **Deep Learning & Unsupervisedlearning.** Autumn 2017
- ps4. **EM, DL & RL.** Autumn 2017