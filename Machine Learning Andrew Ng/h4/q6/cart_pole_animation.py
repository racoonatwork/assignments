from math import sin, cos, pi
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from functools import partial
import matplotlib.animation as animation
# from cart_pole import CartPole, Physics
import numpy as np

from tempfile import NamedTemporaryFile
import base64

VIDEO_TAG = """<video controls>
 <source src="data:video/x-m4v;base64,{0}" type="video/mp4">
 Your browser does not support the video tag.
</video>"""

def anim_to_html(anim):
    if not hasattr(anim, '_encoded_video'):
        with NamedTemporaryFile(suffix='.mp4') as f:
            anim.save(f.name, writer='mencoder', fps=20) #extra_args=['-vcodec', 'libx264'])
            video = open(f.name, "rb").read()
        anim._encoded_video = video.encode("base64")
    
    return VIDEO_TAG.format(anim._encoded_video)

from IPython.display import HTML

def display_animation(anim):
    plt.close(anim._fig)
    return HTML(anim.to_html5_video())
    anim.save('temp.mp4', fps=20, writer='avconv', codec='libx264')
    with NamedTemporaryFile(suffix='.mp4') as f:
        video_tag = '<video controls alt="test" src="data:video/x-m4v;base64,{0}">'.format(base64.b64encode(f.read()).decode('utf-8'))
    plt.close(anim._fig)
    return HTML(video_tag)

class CartPoleAnimation:
    def __init__(self):
        self.fig, self.ax = plt.subplots(1)
        self.line, = self.ax.plot([], [], animated=True)
        self.cart = patches.Rectangle((0 - 0.4, -0.25), 0.8, 0.25,
                                      linewidth=1, edgecolor='k', facecolor='cyan')
        self.base = patches.Rectangle((0 - 0.01, -0.5), 0.02, 0.25,
                                      linewidth=1, edgecolor='k', facecolor='r')
        self.ax.add_patch(self.cart)
        self.ax.add_patch(self.base)

        self.ax.set_xlim(-3, 3)
        self.ax.set_ylim(-0.5, 3.5)
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)

        self.length = 0.7
        self.states = []

    def init(self):
        i, (x, x_dot, theta, theta_dot) = self.states[0]
        X = [x, x + 4 * self.length * sin(theta)]
        Y = [0, 4 * self.length * cos(theta)]
        self.line.set_data(X, Y)
        self.cart.update({'xy': [x-0.4, -0.25]})
        self.base.update({'xy': [x-0.01, -0.5]})
        return self.line, self.cart, self.base,

    def animate(self, state_tuple):
        """
        Given the `state_tuple`, displays the cart-pole system.

        Parameters
        ----------
        state_tuple : tuple
            Continuous vector of x, x_dot, theta, theta_dot
        pause_time : float
            Time delay in seconds

        Returns
        -------
        """
        i, (x, x_dot, theta, theta_dot) = state_tuple
        X = [x, x + 4 * self.length * sin(theta)]
        Y = [0, 4 * self.length * cos(theta)]
        self.line.set_data(X, Y)
        self.cart.update({'xy': [x-0.4, -0.25]})
        self.base.update({'xy': [x-0.01, -0.5]})
        # self.cart.update({'center': [x, 0]})
        # self.base.update({'center': [x, -0.25]})
        x_dot_str, theta_str, theta_dot_str = '\\dot{x}', '\\theta', '\\dot{\\theta}'
        self.time_text.set_text('%d x: %.3f, $%s$: %.3f, $%s$: %.3f, $%s$: %.3f' \
                     % (i, x, x_dot_str, x_dot, theta_str, theta, theta_dot_str, x))
        return self.line, self.time_text, self.cart, self.base

    def play(self, states, pause_time=2.5, html=False):
        self.states = [(i, v) for i, v in enumerate(states)]
        plt.ion()
        ani = animation.FuncAnimation(self.fig, self.animate, self.states,
                                      interval=pause_time * 1000/len(states), blit=True, init_func=self.init, repeat=False)
        if html:
            return display_animation(ani)
        else:
            plt.pause(pause_time)
            plt.show()
            return None


# def test():
#     cart_pole = CartPole(Physics())


#     for i in range(5):
#         x, x_dot, theta, theta_dot = 0.0, 0.0, 0.0, 0.0
#         state_tuple = (x, x_dot, theta, theta_dot)
#         states = [state_tuple]
#         for i in range(100):
#             action = 1 if np.random.uniform() > 0.5 else 0
#             state_tuple = cart_pole.simulate(action, state_tuple)
#             states.append(state_tuple)

#         plt.close('all')
#         a = CartPoleAnimation()
#         a.play(states)
        # a.states = states
        # ani = animation.FuncAnimation(a.fig, a.animate, a.states,
        #                           interval=25, blit=True, init_func=a.init)
        # plt.ion()
        # plt.pause(3)
        # plt.show()



if __name__ == "__main__":
    test()
