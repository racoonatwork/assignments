from collections import defaultdict
import sys
import itertools
import replace_sparse_words


class StateConditionalProbability(object):
    """the probability of state given n previous state"""

    def __init__(self, state_counts):
        super(StateConditionalProbability, self).__init__()
        self.state_counts = state_counts

    def probability_of_state_sequence(self, state_sequence):
        pass

    def process_counts_file(self):
        pass


class TrainedProbability(object):
    """
    calculate different probabilities from preprocess counts file of
    the training data
    """

    def __init__(self, counts_file):
        super(TrainedProbability, self).__init__()
        self.counts_file = counts_file
        # key:state value:(dictionary[key:word value:count])
        self.observation_count_given_state_dict = defaultdict(
            lambda: defaultdict(int))
        self.state_count_dict = defaultdict(int)
        self.state_count_2GRAM_dict = defaultdict(int)
        self.state_count_3GRAM_dict = defaultdict(int)
        self.word_set = set()
        # process count data
        self.process_counts_file()

    def preprocess_observation(self, observation):
        new_observation = observation
        if observation not in self.word_set:
            new_observation = replace_sparse_words.RareWordClass(observation).word_classes()
        return new_observation

    def emission_probability(self, observation, state):
        search_observation = self.preprocess_observation(observation)
        observation_count_given_state = \
            self.observation_count_given_state_dict[state][search_observation]
        state_count = self.state_count_dict.get(state)
        return state_count and float(observation_count_given_state) / state_count or 0

    def conditional_probability(self, state, given_states):
        """
        conditional_probability of state with given_states
        @param given_states should be a tuple of given state
        """
        if len(given_states) >= 3:
            sys.stderr.write("cant handle n-gram when n > 3")
            sys.exit(1)
        elif len(given_states) == 2:
            given_states_count = self.state_count_2GRAM_dict[given_states]
            state_tuple = given_states + (state,)
            state_with_given_states_count = self.state_count_3GRAM_dict[
                state_tuple]
            return given_states_count and \
                   float(state_with_given_states_count) / given_states_count or 0
        elif len(given_states) == 1:
            given_state_count = self.state_count_dict[given_states[0]]
            state_tuple = given_states + (state,)
            state_with_given_states_count = self.state_count_2GRAM_dict[
                state_tuple]
            return given_state_count and \
                   float(state_with_given_states_count) / given_state_count or 0

    def joint_probability(self, state, given_states, observation):
        """
        the joint probablity: P(state | given_states) * P(observation | state)
        :param state: the hidden state of the observation
        :param given_states: previous states
        :param observation: observation
        :return: the joint probablity: P(state | given_states) * P(observation | state)
        """
        return self.conditional_probability(state, given_states) * self.emission_probability(observation, state)

    def process_counts_file(self):
        for line in self.counts_file:
            fields = line.strip().split(" ")
            count = int(fields[0])
            if fields[1] == "WORDTAG":
                self.word_set.add(fields[3])
                self.observation_count_given_state_dict[
                    fields[2]][fields[3]] = count
            elif fields[1] == "1-GRAM":
                self.state_count_dict[fields[2]] = count
            elif fields[1] == "2-GRAM":
                self.state_count_2GRAM_dict[tuple(fields[2:])] = count
            elif fields[1] == "3-GRAM":
                self.state_count_3GRAM_dict[tuple(fields[2:])] = count


def viterbi_with_3gram(observation_list, state_list, start_symbol, stop_symbol, probability_calculator):
    """
    viterbi algorithm
    :param observation_list: a sequence of observations
    :param state_list: the hidden state set
    :param start_symbol: the start symbol indicates the start point of a sentence.
    :param stop_symbol: the stop symbol indicates the stop point of a sentence.
    :param probability_calculator: a trained probability calculator, using it to calculate the emission probability or
    n-gram state transition probability
    :return: the state-path through the HMM which assigns maximum likelihood to the observation sequence
    """
    assert isinstance(probability_calculator, TrainedProbability)
    # 1. create a path probability list which index is the length of observation sequence.
    # each item is a dictionary, the key is the state bigram(state, state) and
    # the value is the maximum probability for observation sequence of length which equals the current index
    # , ending in the state bigram
    maximum_probabilities = [defaultdict(int) for x in range(len(observation_list) + 1)]
    backpointer = [{} for x in range(len(observation_list))]

    # 2. initialization step
    # the maximum probability for observation sequence of length 0,
    # ending in the state bigram(start_symbol, start_symbol) is 1
    maximum_probabilities[0][(start_symbol, start_symbol)] = 1

    # 3. recursion step
    for i, observation in enumerate(observation_list):
        states_for_next_to_last = state_list
        if i == 0:
            states_for_next_to_last = [start_symbol]
        for next_to_last_state, last_state in itertools.product(states_for_next_to_last, state_list):
            maximum_probability = 0
            # find the maximum probability
            states_for_third_to_last = state_list
            if i == 0 or i == 1:
                states_for_third_to_last = [start_symbol]
            for third_to_last_state in states_for_third_to_last:
                probability = maximum_probabilities[i][(third_to_last_state, next_to_last_state)] * \
                    probability_calculator.joint_probability(last_state, (third_to_last_state, next_to_last_state), observation)
                if probability > maximum_probability:
                    maximum_probability = probability
                    assert isinstance(next_to_last_state, str)
                    assert isinstance(maximum_probability, float)
                    maximum_probabilities[i+1][(next_to_last_state, last_state)] = maximum_probability
                    backpointer[i][(next_to_last_state, last_state)] = third_to_last_state
                    # print("i = {0}, probability ({1} | {2} {3}) = {4}", i, third_to_last_state,
                    #       next_to_last_state, last_state, probability)

    # 4. termination step
    best_states = [start_symbol for x in range(len(observation_list))]
    maximum_probability = 0
    for (next_to_last_state, last_state) in itertools.product(state_list, state_list):
        probability = maximum_probabilities[len(observation_list)][(next_to_last_state, last_state)] * \
            probability_calculator.conditional_probability(stop_symbol, (next_to_last_state, last_state))
        if probability > maximum_probability:
            maximum_probability = probability
            best_states[len(observation_list) - 1] = last_state
            best_states[len(observation_list) - 2] = next_to_last_state

    for i in range(len(observation_list) - 3, -1, -1):
        best_states[i] = backpointer[i+2][(best_states[i+1], best_states[i+2])]

    print("input is {0}, output is {1}, maximum probability is {3}".format(observation_list, best_states, maximum_probability))
    return best_states


class DecodingHandler(object):
    """Given the input file which contains the observation sequences,
    write the best matched state sequences to the output file"""

    def __init__(self, trained_probability, input_file_object, output_file_object):
        self.trained_probability = trained_probability
        self.input_file_object = input_file_object
        self.output_file_object = output_file_object

    def decoding(self):
        """decoding the input observation sequences, write the best hidden state sequences to the output file"""

        tagged_corpus = []
        sentence = []
        state_list = ['I-GENE', 'O']
        for line in self.input_file_object:
            if line == '\n':
                if len(sentence) > 0:
                    best_states = viterbi_with_3gram(sentence, state_list, '*', 'STOP', self.trained_probability)
                    for word, state in zip(sentence, best_states):
                        self.output_file_object.write(word + " " + state + "\n")
                self.output_file_object.write('\n')
                sentence = []
            else:
                sentence.append(line[:-1])


def usage_for_part2():
    print("""usage: [python HMM_Assignment_Part2.py parameter_name=parameter_value]
            parameter_name are as follows: count, input, output,""")


if __name__ == '__main__':
    args = dict([arg.split("=", maxsplit=1) for arg in sys.argv[1:]])
    if len(args) != 3:
        usage_for_part2()
        sys.exit(2)
    input_count_file = args.get("count")
    input_file = args.get('input')
    output_file = args.get('output')
    try:
        with open(input_count_file, 'r') as count_file_object, \
             open(input_file, 'r') as input_corpus, \
             open(output_file, 'w') as output_corpus:
            trained_probability = TrainedProbability(count_file_object)
            decoding_handler = DecodingHandler(trained_probability, input_corpus, output_corpus)
            decoding_handler.decoding()
            # fields = input_states.strip().split(" ")
            # if len(fields) < 2:
            #     print("invalid states")
            # else:
            #     input_state = fields[0]
            #     input_given_states = tuple(fields[1:])
            #     probability = trainedProbability.conditional_probability(
            #         input_state, input_given_states)
            #     print(
            #         "{0} | {1} = {2}".format(input_state, input_given_states, probability))
    except IOError as e:
        print(e)
        sys.exit(1)
