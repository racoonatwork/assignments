# coding=utf-8
import sys
from collections import defaultdict


class EmissionProbabilityProcesser:

    def __init__(self, corpus_for_training, sparse_word_symbol):
        # key:word value: dictionary:(key:tag value:countOfTag)
        self.tagCountForWordDictionary = defaultdict(lambda: defaultdict(int))
        self.corpus_for_training = corpus_for_training
        self.tagCountDictionary = defaultdict(int)
        self.sparse_word_symbol = sparse_word_symbol
        self.maxEmissionProbabilityTagForWordDictionary = dict()
        self.hasProcessed = False

    def write_tag_for_corpus_to_file(self, corpus, output):
        """
        get the maximum probability of tag for each word in the corpus,
        then write to the output write_to_file
        """
        if (not self.hasProcessed):
            self.process_emission_probabilty()
            self.hasProcessed = True
        taggedCorpus = []
        for line in corpus:
            if line == '\n':
                taggedCorpus.append(line)
            else:
                word = line[:-1]
                tag = self.maximum_emission_probability_tag_for_word(word)
                taggedCorpus.append(word + " " + tag + "\n")
        with open(output, 'w') as outputfile:
            outputfile.writelines(taggedCorpus)

    def process_emission_probabilty(self):
        """count the tag for each word"""
        for line in self.corpus_for_training:
            fields = line[:-1].split(" ")
            if (len(fields) > 1):
                word = fields[0]
                tag = fields[1]
                tagDictionary = self.tagCountForWordDictionary[word]
                tagDictionary[tag] += 1
                self.tagCountDictionary[tag] += 1

    def emission_probability(self, word, tag):
        """ conditional probability: (word | tag) """
        countOfTag = self.tagCountDictionary[tag]
        countOfWordGivenTag = self.tagCountForWordDictionary[word][tag]
        if countOfTag:
            return float(countOfWordGivenTag) / countOfTag
        else:
            return 0

    def maximum_emission_probability_tag_for_word(self, word):
        """
        Return the tag which has the maximum emission_probability for word
        """
        searchWord = word
        if word not in self.tagCountForWordDictionary:
            searchWord = self.sparse_word_symbol
        if (searchWord in self.maxEmissionProbabilityTagForWordDictionary):
            return self.maxEmissionProbabilityTagForWordDictionary[word]
        maximumProbability = 0
        tagWithMaximumProbability = ""
        for tag, countOfTag in self.tagCountDictionary.items():
            countOfWordGivenTag = self.tagCountForWordDictionary[
                searchWord][tag]
            conditionalProbability = countOfTag and \
                float(countOfWordGivenTag) / countOfTag or 0
            if (maximumProbability <= conditionalProbability):
                maximumProbability = conditionalProbability
                tagWithMaximumProbability = tag

        self.maxEmissionProbabilityTagForWordDictionary[
            word] = tagWithMaximumProbability
        return tagWithMaximumProbability


def usage_for_emission():
    print("""usage: [python emission.py parameter_name=parameter_value]
            parameter_name are as follows: training input sparse output""")

if __name__ == '__main__':
    args = dict([arg.split("=", maxsplit=1) for arg in sys.argv[1:]])
    training_file = args.get("training")
    input_file = args.get("input")
    sparse_word_symbol = args.get("sparse")
    output_file = args.get("output")
    if not (training_file and
            input_file and
            sparse_word_symbol and
            output_file):
        usage_for_emission()
        sys.exit(2)
    try:
        with open(training_file, 'r') as training_corpus, \
             open(input_file, 'r') as input_corpus:
            processer = EmissionProbabilityProcesser(
                training_corpus, sparse_word_symbol)
            processer.write_tag_for_corpus_to_file(input_corpus, output_file)
    except IOError as e:
        print(e)
        sys.exit(1)
