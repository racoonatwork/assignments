# coding=utf-8
import sys
from collections import defaultdict


class SparseWordProcesser:

    def __init__(self, corpus, n):
        """
        If count(word) < n, the word will be replaced with
        the specified symbol
        """
        self.sparse_ceiling = n
        self.corpus = corpus
        self.words_count = defaultdict(int)

    def write_result(self, output):
        """
        wirte the result corpus which has the sparse symbol
        to the output file
        """
        if len(self.words_count) == 0:
            self.count_words()
        new_corpus = self.replace_with_symbol()
        with open(output, 'w') as output_file:
            output_file.writelines(new_corpus)

    def count_words(self):
        for line in self.corpus:
            if not line:
                continue
            fields = line.split(" ")
            words = fields[:-1]
            for word in words:
                self.words_count[word] += 1

    def replace_with_symbol(self):
        sparse_words = set()
        for (word, count) in self.words_count.items():
            if count < self.sparse_ceiling:
                sparse_words.add(word)
        print("sparse wrods count = {0}, all words = {1}".format(
            len(sparse_words), len(self.words_count)))
        new_corpus = []
        self.corpus.seek(0)
        for line in self.corpus:
            if line:
                fields = line.split(" ")
                for index, word in enumerate(fields[:-1]):
                    if word in sparse_words:
                        fields[index] = RareWordClass(word).word_classes()

                new_corpus.append(" ".join(fields))
            else:
                new_corpus.append(line)
        return new_corpus


class RareWordClass:
    """word classes of the rare word."""
    def __init__(self, word):
        assert len(word) > 0
        self.rare_word = word

    def word_classes(self):
        if self.has_numeric_character(self.rare_word):
            return '_NUMERIC_'
        if self.rare_word.isupper():
            return '_ALL_CAPTIALS_'
        if self.rare_word[-1].isupper():
            return '_LAST_CAPTIAL_'
        return "_RARE_"

    def has_numeric_character(self, input_string):
        return any(char.isdigit() for char in input_string)


def usage():
    print("""
        usage: python replace_sparse_words.py [input_file] [output_file]
        """)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
        sys.exit(2)
    try:
        with open(sys.argv[1], 'r') as corpus_file:
            processer = SparseWordProcesser(corpus_file, 5)
            processer.write_result(sys.argv[2])
    except IOError:
        sys.stderr.write("ERROR: Cannot read inputfile %s.\n" % sys.argv[1])
        sys.exit(1)
