from unittest import TestCase
from NLP_h1.HMM_Assignment_Part2 import viterbi
from NLP_h1.HMM_Assignment_Part2 import TrainedProbability
import numpy

class TestViterbi(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_viterbi(self):
        observation_list = [1, 2, 3]
        state_list = ['H', 'C']
        transition_probability_matrix = numpy.matrix('0.8 0.2; 0.7 0.3; 0.4 0.6')
        emission_probability_matrix = numpy.matrix('0.2 0.4 0.4; 0.5 0.4 0.1')

        o1 = [1, 1, 1, 2, 2, 2, 3, 3, 3]
        # o2 = [1, 2, 3, 2, 1]
        # o3 = [1, 3, 1, 3]

        s1 = viterbi(observation_list, state_list, transition_probability_matrix, emission_probability_matrix, o1)
        print(s1)
