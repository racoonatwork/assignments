#!/usr/bin/env python

"""
map infrequent words in the training data to a common class.
"""

from collections import defaultdict
import json
import sys


class RareWordsReplacer:
    """
    replace rare words in the training data to some classes
    """
    def __init__(self, corpus, upper_limit_of_rare_words):
        """
        initialize a rare words replacer with training data and the upper limit for the rare word.
        :param corpus:  the training file object, each line of the file is a single parse tree in Chomsky Normal Form
        :param upper_limit_of_rare_words: if the count of word is not greater than this,
        replace the word to RareWordClass
        """
        self.corpus = corpus
        self.upper_limit = upper_limit_of_rare_words
        # the count of words dictionary
        self.words_count_dict = defaultdict(int)
        self.trees = []

    def count_words(self):
        """count words in the training file object"""
        for line in self.corpus:
            tree = json.loads(line)
            self.__count(tree)

    def is_rare_word(self, word):
        """
        whether the word is rare, return Yes if it's rare
        """
        return self.words_count_dict[word] < self.upper_limit

    def __count(self, tree):
        """count words in a tree"""
        if isinstance(tree, str):
            return

        if len(tree) == 3:
            self.__count(tree[1])
            self.__count(tree[2])
        elif len(tree) == 2:
            word = tree[1]
            self.words_count_dict[word] += 1

    def replace_rare_words(self):
        """repace rare words in the training file object"""
        trees_with_rare_word_classes = []
        self.corpus.seek(0)
        for line in self.corpus:
            tree = json.loads(line)
            self.__replace_rare_word_with_tree(tree)
            self.trees.append(tree)

    def __replace_rare_word_with_tree(self, tree):
        """replace rare word in a tree"""
        if isinstance(tree, str):
            return

        if len(tree) == 3:
            self.__replace_rare_word_with_tree(tree[1])
            self.__replace_rare_word_with_tree(tree[2])
        elif len(tree) == 2:
            word = tree[1]
            if self.words_count_dict[word] < self.upper_limit:
                tree[1] = RareWordClass(word).word_classes()

    def write_output(self, output):
        """write the result to the output stream"""
        for tree in self.trees:
            output.write(json.dumps(tree))
            output.write('\n')


class RareWordClass:
    """word classes of the rare word."""

    def __init__(self, word):
        assert len(word) > 0
        self.rare_word = word

    def word_classes(self):
        # if has_numeric_character(self.rare_word):
        #     return '_NUMERIC_'
        # if self.rare_word.isupper():
        #     return '_ALL_CAPTIALS_'
        # if self.rare_word[-1].isupper():
        #     return '_LAST_CAPTIAL_'
        return "_RARE_"


def has_numeric_character(input_string):
    return any(char.isdigit() for char in input_string)


def usage():
    return "python p1.py train_file > output_file"

if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage()
        sys.exit(1)
    train_file = sys.argv[1]
    with open(train_file, 'r') as train_file_object:
         replacer = RareWordsReplacer(train_file_object, 5)
         replacer.count_words()
         replacer.replace_rare_words()
         replacer.write_output(sys.stdout)