#!/usr/bin/env python

"""
PCFG: probabilistic context-free grammar or stochastic context-free grammar.
It can be defined by a quintuple: G = (N, T, R, S, q).
where
   N: the set of non-terminal symbols
   T: the set of terminal symbols.
   R: the rule of the form X -> Y1Y2...Yn, where X is in N, Yi is in (N and T) for i in 1..n
   S: the start symbol in the set N
   q: the rule parameters. a parameter q(X->Y) is the probability of choosing X->Y when given the X

CYK algorithm: Cocke-Younger-Kasami algorithm.
a parsing algorithm for PCFG.
"""
from collections import defaultdict
import itertools
import sys
import json
from h2.assignment.p1 import RareWordClass
import timeit
import re
import cProfile


class PCFG:
    """probabilistic context-free grammar"""
    def __init__(self, count_file_object):
        self.non_terminal_count = defaultdict(int)
        self.unary_rules_count = defaultdict(int)
        self.binary_rules_count = defaultdict(int)

        self.non_terminal_symbols = set()
        self.unary_rules = defaultdict(list)
        self.binary_rules = defaultdict(list)
        self.word_set = set()

        self.__read_count_file(count_file_object)

    @classmethod
    def start_symbol(cls):
        return "SBARQ"

    def __read_count_file(self, count_file_object):
        for line in count_file_object:
            fields = line.strip().split(" ")
            count = int(fields[0])
            # non-terminal symbol
            if fields[1] == "NONTERMINAL":
                self.non_terminal_symbols.add(fields[2])
                self.non_terminal_count[fields[2]] = count
            # unary rule
            elif fields[1] == "UNARYRULE":
                self.unary_rules[fields[2]].append(fields[3])
                self.unary_rules_count[(fields[2], fields[3])] = count
                self.word_set.add(fields[3])
            # binary rule
            elif fields[1] == "BINARYRULE":
                self.binary_rules[fields[2]].append((fields[3], fields[4]))
                self.binary_rules_count[(fields[2], fields[3], fields[4])] = count

    def probability_of_binary_rule(self, binary_rule_symbols):
        assert len(binary_rule_symbols) == 3
        count_of_non_terminal = self.non_terminal_count[binary_rule_symbols[0]]
        if count_of_non_terminal == 0:
            return 0
        return self.binary_rules_count[binary_rule_symbols] / count_of_non_terminal

    def probability_of_unary_rule(self, unary_rule_symbols):
        assert len(unary_rule_symbols) == 2
        count_of_non_terminal = self.non_terminal_count[unary_rule_symbols[0]]
        if count_of_non_terminal == 0:
            return 0
        word = unary_rule_symbols[1]
        if word not in self.word_set:
            word = RareWordClass(word).word_classes()
        return self.unary_rules_count[(unary_rule_symbols[0], word)] / count_of_non_terminal


def cyk(sentence, pcfg):
    """
    a parsing algorithm for pcfg
    :param sentence: the sentence to be parsed
    :param pcfg: PCFG
    :return: the parse tree of the sentence with the highest score
    """
    assert isinstance(pcfg, PCFG)
    # 1. create a dictionary "max_scores", its key is tuple(i, j, X) where i <= j and X is non-terminal symbol,
    # its value is the score for the maximum probability parse tree for words Si...Sj
    # with non-terminal X as the parse tree's root.
    max_scores = defaultdict(int)
    # (i, j, X) -> (k, Y, Z) where k in range(i, j), X->YZ
    backpointer = defaultdict(tuple)
    words = sentence.strip().split(' ')
    word_count = len(words)

    # 2.initialization step
    for i in range(word_count):
        for X in pcfg.non_terminal_symbols:
            max_scores[(i, i, X)] = pcfg.probability_of_unary_rule((X, words[i]))

    # 3.recursive step
    # would there be word_count <= 2 in Chmosky Normal Form?
    for span in range(1, word_count):
        for i in range(0, word_count - span):
            j = i + span
            for X in pcfg.non_terminal_symbols:
                max_probability = 0
                for k, (Y, Z) in itertools.product(range(i, j), pcfg.binary_rules[X]):
                    max_scoreY = max_scores[(i, k, Y)]
                    if max_scoreY == 0:
                        continue
                    max_scoreZ = max_scores[(k+1, j, Z)]
                    if max_scoreZ == 0:
                        continue
                    probability = pcfg.probability_of_binary_rule((X, Y, Z)) * max_scoreY * max_scoreZ
                    if probability > max_probability:
                        max_probability = probability
                        max_scores[(i, j, X)] = max_probability
                        backpointer[(i, j, X)] = (k, Y, Z)

    # 4. termination step
    start_point = 0
    end_point = word_count - 1
    start_symbol = PCFG.start_symbol()
    another_start_symbol = 'S'
    if max_scores[(start_point, end_point, another_start_symbol)] > max_scores[(start_point, end_point, PCFG.start_symbol())]:
        start_symbol = another_start_symbol
    if max_scores[(start_point, end_point, start_symbol)] == 0:
        print("the max score is zero, no matched parse tree! sentence = ", sentence)
    parse_tree = [start_symbol, [], []]
    recover_parse_tree(parse_tree, start_point, end_point, backpointer, sentence)
    return parse_tree


def recover_parse_tree(tree, start, end, backpointer, sentence):
    """a helper function. recover the parse tree from backpointer """
    assert isinstance(tree, list)
    if start == end:
        words = sentence.strip().split(' ')
        tree[:] = [tree[0], words[start]]
        return
    bp = backpointer[(start, end, tree[0])]
    if len(bp) != 3:
        print(
            'unexcpeted backpointer!, bp = {0}, sentence = {1}, '
            'tree={2}, start={3}, end={4}'.format(bp, sentence, tree, start, end))
        assert False
    split = bp[0]
    # first non-terminal symbol
    first_non_terminal_symobl = bp[1]
    tree[1] = [first_non_terminal_symobl, [], []]
    recover_parse_tree(tree[1], start, split, backpointer, sentence)
    # second non-terminal symbol
    second_non_terminal_symobl = bp[2]
    tree[2] = [second_non_terminal_symobl, [], []]
    recover_parse_tree(tree[2], split+1, end, backpointer, sentence)


def parse(count_file, corpus_file, output, has_vertical_symbol = False):
    trees = []
    with open(count_file, 'r') as count_file_object, \
         open(corpus_file, 'r') as corpus_file_object:
        pcfg = PCFG(count_file_object)
        for sentence in corpus_file_object:
            if sentence == '\n':
                continue
            trees.append(cyk(sentence, pcfg))
    for tree in trees:
        json_tree = json.dumps(tree)
        if has_vertical_symbol:
            json_tree = re.sub(r'\^<.*?>', '', json_tree)
        output.write(json_tree)
        output.write('\n')


def usage():
    print("usage: python p2.py count_file corpus_file output_file")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        usage()
        print(sys.argv)
        sys.exit(1)
    with open(sys.argv[3], 'w') as output_file_object:
        cProfile.run('parse(sys.argv[1], sys.argv[2], output_file_object, has_vertical_symbol=True)', sort='time')
    # parse(sys.argv[1], sys.argv[2], sys.stdout, has_vertical_symbol=True)
    # t = timeit.Timer('parse(sys.argv[1], sys.argv[2], sys.stdout)', setup='from __main__ import parse')
    # duration = t.timeit(number=1)
