from unittest import TestCase
from h2.assignment import p1
import sys
import filecmp


class TestRareWordsReplacer(TestCase):
    def setUp(self):
        train_file_object = open('test_train.dat', 'r')
        self.replacer = p1.RareWordsReplacer(train_file_object, 2)

    def tearDown(self):
        del self.replacer

    def test_count_words(self):
        self.replacer.count_words()
        self.assertTrue(self.replacer.is_rare_word('laughs'))
        self.assertTrue(self.replacer.is_rare_word('barks'))
        self.assertFalse(self.replacer.is_rare_word('The'))
        self.assertFalse(self.replacer.is_rare_word('dog'))

    def test_replace_rare_words(self):
        self.replacer.count_words()
        self.replacer.replace_rare_words()

        result_filename = 'test_train.result'
        result_file = open(result_filename, 'w')
        self.replacer.write_output(result_file)
        result_file.close()
        self.assertTrue(filecmp.cmp(result_filename, 'test_train_rare.key'))
        self.assertEqual(self.replacer.trees[0][2][1][1], '_RARE_')
        # self.assertMultiLineEqual()

