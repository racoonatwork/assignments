# start symbol indicates the begining of a sentence
START_SYMBOL = '*'
# end symbol indicates the end of a sentence
STOP_SYMBOL = 'STOP'

# valid tag list
TAG_LIST = ['O', 'I-GENE']
