# suffix
2016-08-11 11:34:18
## Description:
> the first experiment with suffix feature.

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
765 | 642 | 361 | 0.471895 | 0.562305 | 0.513149
    

--------------------------
# suffix
2016-08-11 11:46:34
## Description:
> the first experiment with suffix feature.

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
765 | 642 | 361 | 0.471895 | 0.562305 | 0.513149
    

--------------------------
# alpha
2016-08-17 14:36:35
## Description:
> test alpha feature

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3
- Alpha

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
689 | 642 | 359 | 0.521045 | 0.55919 | 0.539444
    

--------------------------
# prefix
2016-08-17 14:58:22
## Description:
> test prefix feature without alpha feature

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3
- Prefix_1
- Prefix_2
- Prefix_3

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
1163 | 642 | 410 | 0.352537 | 0.638629 | 0.454294
    

--------------------------
# capitalization
2016-08-19 23:22:33
## Description:
> test capitalization feature

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3
- Capitalization

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
1072 | 642 | 364 | 0.339552 | 0.566978 | 0.424737
    

--------------------------
# capitalization of non-first word
2016-08-20 09:58:26
## Description:
> test capitalization of non-first word feature

## Features:
- Trigram
- WordTag
- Suffix_1
- Suffix_2
- Suffix_3
- CapitalizationNotFirstWord

## Evaluation:

Found | Expected | Correct | Precision | Recall | F1-Score
--- | --- | --- | --- | --- | ---
772 | 642 | 321 | 0.415803 | 0.5 | 0.454031
    

--------------------------
