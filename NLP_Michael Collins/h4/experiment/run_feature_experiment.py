#! /bin/usr/python

""" This is a script for convenience. Modify features then run this to show the result.

When modify the feature.py to add, remove or update some features,
run this script to do tasks one by one as below:
1. run perceptron algorithm. This step will recompute the weight vector for new features
2. run Viterbi algorithm. This step will decode the development data.
3. run eval_gene_tagger.py. This step will print the evaluation of the decoding result.
4. run look_error.py. This step will print the differences between the labeled data and the output data.
:TODO every experiment of features has a distinct folder that saves its output and evaluation?
"""

import contextlib
import io
import os
import sys

import eval_gene_tagger
import p1
from experiment import experiment_report
from feature import FeaturesExperiment
import perceptron


def usage():
    print("usage: python run_feature_experiment.py training weight_output dev prediction_file key_file")


def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) != 5:
        usage()
        return 2

    # 1. crate the experiment folder from the experiment name
    experiment_path = create_experiment_folder(FeaturesExperiment.name())

    # 2. parameters
    training_file = argv[1]
    weight_output = os.path.join(experiment_path, FeaturesExperiment.feature_weight_filename())
    dev_file = argv[2]
    predication_file = os.path.join(experiment_path, FeaturesExperiment.feature_predication_filename(argv[2]))
    key_file = argv[3]
    dirname = os.path.dirname(os.path.realpath(__file__))
    experiment_history_file = os.path.join(dirname, argv[4])

    # 1. run perceptron algorithm.
    result = perceptron.write_weight_vector(training_file, weight_output)

    # 3. run Viterbi algorithm, write the prediction_file to the corresponding folder
    p1.decode_from_file(weight_output, dev_file, predication_file)

    # 4. run eval_gene_tagger
    evaluation_string_io = io.StringIO()
    with contextlib.redirect_stdout(evaluation_string_io):
        eval_gene_tagger.evaluate(key_file, predication_file)

    # 5. generate the experiment report
    report_file = os.path.join(experiment_path, FeaturesExperiment.experiment_report_filename())
    experiment_report.generate_report(evaluation_string_io.getvalue(), predication_file, key_file,
                                      report_file, experiment_history_file)

    return 0


def create_experiment_folder(experiment_name):
    """create a file folder which contains the experiment result

    :param experiment_name: the name of a experiment
    :return: the relative file path of a experiment
    """
    dirname = os.path.dirname(os.path.realpath(__file__))
    experiment_path = os.path.join(dirname, experiment_name)
    if not os.path.exists(experiment_path):
        os.mkdir(experiment_path)
    return experiment_path


if __name__ == '__main__':
    sys.exit(main())
