"""
Binary local feature g(h,t) in the Global Linear Model.
h : the input history (tag_2, tag_1, sentence, index)
t : the output tag
"""
import constant
import typing


class HistoryBasedBinaryFeature(object):
    """
    a binary feature which checks whether the feature is fulfilled
    """

    def __init__(self, history, tag):
        """
        init a feature with history(tag_2, tag_1, sentence, index) and tag
        :param history: (tag_2, tag_1, sentence, index)
        :param tag: the output tag at index
        """
        self.tag_2 = history[0]
        self.tag_1 = history[1]
        self.sentence = history[2]
        self.index = history[3]
        self.tag = tag

    # def is_fulfilled(self):
    #     """
    #     check whether the feature is fulfilled
    #     :return: return yes if the feature is fulfilled, return no otherwise.
    #     """
    #     return True

    def feature_name(self):
        """the feature name. It can be used by the experiment report"""
        return ""

    def feature_key(self):
        """
        we implement the feature vector as a map from strings to counts. Since the most
        features are 0 for a given history.
        :return the string as the key of this feature. return empty string if no such feature key
        """
        return ""


class TrigramFeature(HistoryBasedBinaryFeature):
    def feature_name(self):
        return "Trigram"

    def feature_key(self):
        return ":".join(["TRIGRAM", self.tag_2, self.tag_1, self.tag])


class WordTagFeature(HistoryBasedBinaryFeature):
    def feature_name(self):
        return "WordTag"

    def is_fulfilled(self):
        return self.index < len(self.sentence)

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""
        return ":".join(["TAG", self.sentence[self.index], self.tag])


class SuffixFeature(HistoryBasedBinaryFeature):
    def __init__(self, history, tag, length):
        """
        word suffixes feature, It's a useful feature indicator as a word class.
        :param history: (tag_2, tag_1, sentence, index)
        :param tag: the output tag at index
        :param length: the length of word suffix
        """
        super(SuffixFeature, self).__init__(history, tag)
        self.suffix_length = length

    def feature_name(self):
        return "Suffix_" + str(self.suffix_length)

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""
        word = self.sentence[self.index]
        if self.suffix_length >= len(word):
            return ""
        suffix = word[-self.suffix_length:]
        return ":".join(["SUFFIX", suffix, self.tag])


class PrefixFeature(HistoryBasedBinaryFeature):
    def __init__(self, history, tag, length):
        """
        word prefixes feature, It's a useful feature indicator as a word class.
        :param history: (tag_2, tag_1, sentence, index)
        :param tag: the output tag at index
        :param length: the length of word suffix
        """
        super(PrefixFeature, self).__init__(history, tag)
        self.suffix_length = length

    def feature_name(self):
        return "Prefix_" + str(self.suffix_length)

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""
        word = self.sentence[self.index]
        prefix = word[0:self.suffix_length]
        return ":".join(["PREFIX", prefix, self.tag])


class CapitalizationFeature(HistoryBasedBinaryFeature):
    def feature_name(self):
        return "Capitalization"

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""
        word = self.sentence[self.index]
        if len(word) and 'A' <= word[0] <= 'Z':
            return ":".join(["Cap", self.tag])
        return ""


class CapitalizationNonFirstWordFeature(HistoryBasedBinaryFeature):
    """Capitalization of words that aren't the first letter of a sentence usually indicates a proper noun"""
    def feature_name(self):
        return "CapitalizationNotFirstWord"

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""
        if self.index == 0:
            return ""
        word = self.sentence[self.index]
        if len(word) and 'A' <= word[0] <= 'Z':
            return ":".join([self.feature_name(), self.tag])
        return ""


def is_all_non_alpha(word: typing.List[str]):
    """Return True if all bytes in the word are non-alpha ASCII characters"""
    for character in word:
        if character.isalpha():
            return False
    return True


class AlphaFeature(HistoryBasedBinaryFeature):
    def feature_name(self):
        return "Alpha"

    def feature_key(self):
        if self.index >= len(self.sentence):
            return ""

        word = self.sentence[self.index]
        if is_all_non_alpha(word):
            return "AllNonAlpha:" + self.tag
        else:
            return "HasAlpha:" + self.tag



class FeaturesExperiment(object):
    """This class contains the features used by the experiment, and
    other experiment information like its name, description et al.
    """

    # the list of all feature name
    feature_name_list = list()

    @classmethod
    def name(cls):
        """the name of this experiment"""
        return "capitalization of non-first word"

    @classmethod
    def description(cls):
        """the description of this experiment"""
        return "test capitalization of non-first word feature"

    @classmethod
    def feature_weight_filename(cls):
        """the name of the weight vector file"""
        return ".".join(['tag', 'model'])

    @classmethod
    def feature_predication_filename(cls, devset_filename):
        """the predication file name is a word sequences separated by delimiter '.'

        :param devset_filename: the filename of development set
        """
        return ".".join([devset_filename, 'out'])

    @classmethod
    def experiment_report_filename(cls):
        return "report.html"

    @classmethod
    def get_all_local_features(cls, history, tag) -> typing.List[HistoryBasedBinaryFeature]:
        """
        return all local features as g(history, tag) used by GLM
        :param history: (tag_2, tag_1, sentence, index)
        :param tag: the output tag at index
        """
        feature_list = list()
        # Trigram feature
        feature_list.append(TrigramFeature(history, tag))

        # Word tag feature
        feature_list.append(WordTagFeature(history, tag))

        # Word suffix feature
        for l in range(1, 4):
            feature_list.append(SuffixFeature(history, tag, l))

        # capitalization feature
        # feature_list.append(CapitalizationFeature(history, tag))

        # capitalization with non-first word feature
        feature_list.append(CapitalizationNonFirstWordFeature(history, tag))

        # Word prefix feature
        # for l in range(1, 4):
        #     feature_list.append(PrefixFeature(history, tag, l))

        # Alpha feature
        # feature_list.append(AlphaFeature(history, tag))

        # save the feature names
        if not cls.feature_name_list:
            for feature in feature_list:
                cls.feature_name_list.append(feature.feature_name())

        return feature_list


def get_all_local_features(history, tag) -> typing.List[HistoryBasedBinaryFeature]:
    """
    return all local features as g(history, tag) used by GLM
    :param history: (tag_2, tag_1, sentence, index)
    :param tag: the output tag at index
    """
    feature_list = list()
    # Trigram feature
    feature_list.append(TrigramFeature(history, tag))

    # Word tag feature
    feature_list.append(WordTagFeature(history, tag))

    # Word suffix feature
    for l in range(1, 4):
        feature_list.append(SuffixFeature(history, tag, l))

    return feature_list


def sum_of_inner_product(weight_vector, history, tag):
    """
    calculate v * g(h,t) for all features, * indicates dot product of vector
    :param weight_vector: pre-trained weight vector parameter
    :param history: a 4-tuple (tag-2, tag-1, sentence, index) indicts the history context
    :param tag: the output tag at the index
    :return: the sum of all features that are based on the history given the tag
    """
    feature_list = FeaturesExperiment.get_all_local_features(history, tag)

    # calculate the sum
    scalar = 0
    for feature in feature_list:
        if not feature.feature_key():
            continue
        scalar += weight_vector.get(feature.feature_key(), 0)

    return scalar


def update_weight_vector(weight_vector, golden_tagging, best_tagging):
    """
    update weight vector: v = v + f(x, y) - f(x, z).
           v is weight vector,
           f(x,y) is the golden tagging feature vector,
           f(x, z) is the best tagging feature vector
    :param weight_vector: the weight vector to be updated
    :param golden_tagging: (word, tag) sequence. read it from the training data
    :param best_tagging: (word, tag) sequence. learn this by viterbiGLM algorithm with the weight vector
    :return: the updated weight vector.
    """
    sentence = [word_tag_tuple[0] for word_tag_tuple in golden_tagging]
    for i, (golden, best) in enumerate(zip(golden_tagging, best_tagging)):
        # the tag at index i - 2
        tag_2_golden = constant.START_SYMBOL
        tag_2_best = constant.START_SYMBOL
        if i-2 >= 0:
            tag_2_golden = golden_tagging[i-2][1]
            tag_2_best = best_tagging[i-2][1]

        # the tag at index i - 1
        tag_1_golden = constant.START_SYMBOL
        tag_1_best = constant.START_SYMBOL
        if i-1 >= 0:
            tag_1_golden = golden_tagging[i-1][1]
            tag_1_best = best_tagging[i-1][1]

        # 1. set history parameter h
        history_golden = (tag_2_golden, tag_1_golden, sentence, i)
        history_best = (tag_2_best, tag_1_best, sentence, i)

        # 2. set tag parameter t
        tag_golden = golden[1]
        tag_best = best[1]

        # 3. set parameter f(x,y) , f(x,z)
        golden_feature_list = FeaturesExperiment.get_all_local_features(history_golden, tag_golden)
        best_feature_list = FeaturesExperiment.get_all_local_features(history_best, tag_best)

        # 4. compute v = v + f(x, y)
        for golden_feature in golden_feature_list:
            feature_key = golden_feature.feature_key()
            weight_vector[feature_key] = weight_vector.get(feature_key, 0) + 1

        # 5. compute v = v - f(x, y)
        for best_feature in best_feature_list:
            feature_key = best_feature.feature_key()
            weight_vector[feature_key] = weight_vector.get(feature_key, 0) - 1
