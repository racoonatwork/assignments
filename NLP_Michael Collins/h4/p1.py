#! /usr/bin/python

"""
a trigram tagging decoder with a pre-trained model.
The decoder is based on the global linear model(GLM), whose feature
function f decompose into the sum of local feature over each history/tag pair g(h,t).
"""

import sys
import itertools
import feature
import constant


def read_weight_vector(weight_vector):
    """ read the pre-trained weight vector data into a
    map from feature string to weights, feature string is something like this: "TRIGRAM:O:O:I-GENE"

    :param weight_vector: the file object which records the pre-trained feature weight vector
    :return {feature_string: weight}
    """
    vector = {}
    for line in weight_vector:
        fields = line.strip().split(' ')
        vector[fields[0]] = float(fields[1])
    return vector


def viterbiGLM(weight_vector, sentence):
    """ find the maximum score tag sequence of the sentence.
     Use the similar dynamic algorithm as the HMM viterbi algorithm.

    :param weight_vector: pre-trained weight vector
    :param sentence: the input sentence
    :return: labeled sentence as [(word, tag), ...]
    """
    # 1. initialization step
    # its key is (tag(i-2), tag(i-1), l), l is the length of the tag sequence, l = i + 1 in this case.
    # its value is the maximum score of the tag sequence which lenght is i+1
    start_symbol = constant.START_SYMBOL
    stop_symbol = constant.STOP_SYMBOL
    viterbi = {}
    viterbi[start_symbol, start_symbol, 0] = 0
    # backtrack point
    bt = {}

    # 2.recursion step
    tag_list = constant.TAG_LIST
    for i, word in enumerate(sentence):
        tag_list_i_2 = tag_list if i > 1 else [start_symbol]
        tag_list_i_1 = tag_list if i > 0 else [start_symbol]
        for tag_i_1, tag in itertools.product(tag_list_i_1, tag_list):
            maximum_score = float('-inf')
            for tag_i_2 in tag_list_i_2:
                history = (tag_i_2, tag_i_1, sentence, i)
                if (tag_i_2, tag_i_1, i) not in viterbi:
                    print("none ", tag_i_2, tag_i_1, i)
                score = viterbi[tag_i_2, tag_i_1, i] + feature.sum_of_inner_product(weight_vector, history, tag)
                if score > maximum_score:
                    maximum_score = score
                    viterbi[tag_i_1, tag, i+1] = score
                    bt[tag_i_1, tag, i+1] = tag_i_2

    # 3. termination step
    maximum_score = float('-inf')
    best_tags = [start_symbol for x in range(len(sentence))]
    for tag_i_2, tag_i_1 in itertools.product(tag_list, tag_list):
        history = (tag_i_2, tag_i_1, sentence, len(sentence))
        score = viterbi.get((tag_i_2, tag_i_1, len(sentence)),0) + \
                feature.sum_of_inner_product(weight_vector, history, stop_symbol)
        if score > maximum_score:
            maximum_score = score
            best_tags[len(sentence) - 1] = tag_i_1
            best_tags[len(sentence) - 2] = tag_i_2
    for i in range(len(sentence) - 3, -1, -1):
        best_tags[i] = bt[best_tags[i+1], best_tags[i+2], i+3]

    return [item for item in zip(sentence, best_tags)]


def decode(weight_vector, input_file_object, output_file_object):
    """ decode the input sentences and write the result to the output file

    :param input_file_object: a file object contains the sentences
    :param output_file_object: the decoded tag sequences will be write to the file object
    :return: None
    """
    sentence = []
    result = []
    for line in input_file_object:
        # find the input sentence
        if line != '\n':
            sentence.append(line.strip())
            continue
        if len(sentence) == 0:
            continue
        # decode the sentence
        word_tags = viterbiGLM(weight_vector, sentence)
        result.append(word_tags)
        # clean the sentence
        sentence = []

    for word_tags in result:
        for word_tag_tuple in word_tags:
            output_file_object.write(" ".join(word_tag_tuple))
            output_file_object.write('\n')
        output_file_object.write('\n')


def decode_from_file(weight_vector_file, input_file, output_file):
    with open(weight_vector_file, 'r') as weight_vector, \
            open(input_file, 'r') as input, \
            open(output_file, 'w') as output:
        v = read_weight_vector(weight_vector)
        decode(v, input, output)


def usage():
    print ("usage: python p1.py weight_vector input output ")


def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) != 4:
        usage()
        return 2
    decode_from_file(argv[1], argv[2], argv[3])
    return 0


if __name__ == "__main__":
    sys.exit(main())
