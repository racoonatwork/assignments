"""
Perceptron algorithm: learn the weight vector with the feature vector for the training data.
"""


import p1
import itertools
import feature
import sys
import constant
import getopt
import typing


def learn_weight_vector(training_data, viterbiGLM, iteration_number) -> typing.Dict:
    """ learn the weight vector from the training data

    :param training_data: the training data with labeled words.
    :param viterbiGLM: gives the maximum score "word tag" sequence under the given weight vector
    :param iteration_number: when the number of iteration completed, return the weight vector
    :return: a weight vector which is implemented as a map from feature string to weight
    """
    weight_vector = dict()

    while iteration_number > 0:
        iteration_number -= 1
        # initialize the golden tagging list and the best tagging list
        golden_tagging = list()
        for line in training_data:
            # 1. read the golden tagging
            fields = line.strip().split(' ')
            if len(fields) == 2:
                golden_tagging.append((fields[0], fields[1]))
                continue
            if len(golden_tagging) == 0:
                continue
            # 2. compute the best tagging
            sentence = [word_tag_tuple[0] for word_tag_tuple in golden_tagging]
            best_tagging = viterbiGLM(weight_vector, sentence)
            # 3. update weight vector: v = v + f(x, y) - f(x, z).
            #       v is weight vector,
            #       f(x,y) is the golden tagging feature vector,
            #       f(x, z) is the best tagging feature vector
            golden_tagging.append(("", constant.STOP_SYMBOL))
            best_tagging.append(("", constant.STOP_SYMBOL))
            feature.update_weight_vector(weight_vector, golden_tagging, best_tagging)

            # 4. clear the golden_tagging for next reading
            golden_tagging.clear()

    return weight_vector


def write_weight_vector(training_file, output_file):
    with open(training_file, 'r') as training, \
         open(output_file, 'w') as output:
        trained_weight_vector = learn_weight_vector(training, p1.viterbiGLM, iteration_number=5)
        for key, weight in trained_weight_vector.items():
            if weight == 0:
                continue
            output.write(" ".join([key, str(weight)]))
            output.write('\n')



def usage():
    print("usage: python perceptron.py training output")


def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) != 3:
        usage()
        return 2
    write_weight_vector(argv[1], argv[2])
    return 0


if __name__ == '__main__':
    sys.exit(main())
