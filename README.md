# 课程列表
- [Machine Learning](http://cs229.stanford.edu) by  Andrew Ng, 资料来源于 cs229.stanford.edu
- [NLP](http://www.cs.columbia.edu/~cs4705/) by [Michael Collins](http://www.cs.columbia.edu/~mcollins/)，学习该课程是在coursera上, 但该课程已经不存在，目前链接指向columbia，其内容基本一致。
- [cs224n](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1184/syllabus.html) 2018年的课程

每门课信息的具体介绍和作业来源参见每个课程下的 README.md
