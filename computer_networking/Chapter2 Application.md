# Application

## Review

### R2. What is the difference between network architecture and application architecture?

常见的多个端系统网络应用架构：客户端-服务器(CS), 端到端(P2P); 以参与到通信中的角色来划分。比如发起通信的为客户端，等待请求的为服务器；
不同于网络架构

### R8. List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service

1. 数据可靠
2. 时延保证
3. 吞吐量
4. 安全

TCP：提供可靠的数据传输，保证数据完整且按照发送顺序到达目的主机。另外，还提供拥塞控制，以及面向连接的服务。
UDP：不提供可靠的数据传输，不保证时延，不保证吞吐量以及提供安全服务。仅仅完成传输层的主要任务：为应用层提供端到端的通信。

## Problems

## Programming

代码库在github [network](https://github.com/antatwork/learn_network)