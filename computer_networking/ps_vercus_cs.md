# [ps_vercus_cs](http://gaia.cs.umass.edu/kurose_ross/interactive/ps_versus_cs.php)

Quantitative Comparison of Packet Switching and Circuit Switching

>  Consider the two scenarios below:
>
> - A circuit-switching scenario in which $N_{cs}$ users, each requiring a bandwidth of 20 Mbps, must share a link of capacity 200 Mbps.
> - A packet-switching scenario with $N_{ps}$ users sharing a 200 Mbps link, where each user again requires 20 Mbps when transmitting, but only needs to transmit 20 percent of the time.

## Questions

> a. When circuit switching is used, what is the maximum number of circuit-switched users that can be supported? Explain your answer.

A: 200 / 20 = 10 maximum users.

> b.For the remainder of this problem, suppose packet switching is used. Suppose there are 19 packet-switching users (i.e., $N_{ps}$ = 19). Can this many users be supported under circuit-switching? Explain.

A: 不能，由于带宽最高只能支持10个用户

> c.What is the probability that a given (specific) user is transmitting, and the remaining users are not transmitting?

A: 假定用户是否传输数据是独立的，则$N$个用户，只有一个正在传输的概率为 $0.2 * 0.8^{(N-1)}$

> d. What is the probability that one user (any one among the 19 users) is transmitting, and the remaining users are not transmitting? When one user is transmitting, what fraction of the link capacity will be used by this user?

A:根据上式，当N为20时概率为0.00288, 当该用户传输时，使用了1/10的链路带宽

> e. What is the probability that any 10 users (of the total 19 users) are transmitting and the remaining users are not transmitting? (Hint: you will need to use the binomial distribution [[1](http://www.youtube.com/watch?v=O12yTz_8EOw), [2](http://en.wikipedia.org/wiki/Binomial_distribution)]).

A: 根据独立的假设，概率为 $\binom{19}{10} 0.2^{10} * 0.8^9 = 0.0012696$

> f. What is the probability that more than 10 users are transmitting? Comment on what this implies about the number of users supportable under circuit switching and packet switching.

A: 可以估计11，12..., 19个人正在传输的概率之和小于8* 0.0012696 < 0.01, 所以电路交换虽然只能支持最多10个人在线，但可以保证99%以上的使用情况。而分组交换可以支持所有的19人同时传输数据，并且在大部分人数较少（期望为$19*0.2 = 3.8$)的使用情况时，可以提供更高的带宽