# Compiler

[cs143-compiler](http://web.stanford.edu/class/cs143/)的作业, 作业资源在教授的主页找到的.

## 启动项目

启动项目cooldist可以参考[cooldist](http://theory.stanford.edu/~aiken/software/cooldist/), 可以使用wget递归下载:

```shell
wget -r -np -nH -R "index.html*" http://theory.stanford.edu/~aiken/software/cooldist/
```

参数的大致意思:

1. -r/--recursive: 递归下载
2. -np/--no-parent: 不去下载父目录
3. -nH/--no-host-directories: 不从host开始创建目录,即 ~aiken/software 不会被创建
4. -R rejlist --reject rejlist: 不下载的文件

stduent-dist来自于[Stanford CS143 Compilers - Cool compiler](https://github.com/afterthat97/cool-compiler/tree/62ffa66fcce3d4b62257719e4dd980fb346292de)中README.md的链接