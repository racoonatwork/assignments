(*
 *  CS164 Fall 94
 *
 *  Programming Assignment 1
 *    Implementation of a simple stack machine.
 *
 *  Skeleton file
 *)

class StackNode {
  content_ : String;
  next_ : StackNode; -- 下面的结点

  isNil() : Bool { false };
  content() : String { content_ };
  next(): StackNode { next_ };
  init(s: String, next : StackNode): StackNode {
    {
      content_ <- s;
      next_ <- next;
      self;
    }
  };

};

class NilStackNode inherits StackNode {
  isNil() : Bool { true };
  next(): StackNode { { abort(); self; } };
};


class Stack {
  top_: StackNode <- new NilStackNode;
  size_ : Int <- 0;

  empty() : Bool { top_.isNil()};

  size() : Int { size_ };

  push(s: String): String {
    {
      top_ <- new StackNode.init(s, top_);
      size_ <- size_ + 1;
      s;
    }
  };

  pop(): String {
    {
      if empty() then { abort(); "";} else 
        (let tmp : String <- top_.content() in
          {
            top_ <- top_.next();
            size_ <- size_ -1;
            tmp;
          }
        )
      fi;
    }
  };

  top(): String { top_.content() };

  display(): IO {
    (let curNode : StackNode <- top_, io : IO <- new IO in
      {
        while not curNode.isNil() loop
          {
            io.out_string(curNode.content());
            curNode <- curNode.next();
          }pool;
        io.out_string("\n");
        io;
      }
    )
  };
};

class Command {
  cmd_ : String;
  execute(st : Stack): SELF_TYPE {
    { 
      abort();
      self;
    }
  };

  init(s : String) : Command {
    {
      cmd_ <- s;
      self;
    }
  };
};


-- 将命令本身push到栈中
class PushCommand inherits Command {
  execute(st : Stack) : SELF_TYPE {
    {
      st.push(cmd_);
      self;
    }
  };
};


class EvaluteCommand inherits Command {

  -- 交换top和top的下一个结点
  swap_top(st: Stack) : String {
    if st.size() < 2 then {abort(); "";} else
      (let tmpTopContent : String <- st.pop(), tmpNextContent : String <- st.pop() in
        {
          -- (new IO).out_string("swap_top ".concat(tmpTopContent).concat(" ").concat(tmpNextContent).concat("\n"));
          st.push(tmpTopContent);
          st.push(tmpNextContent);
          st.top();
        }
      )
    fi
  };

  add_top(st: Stack) : String {
    if st.size() < 2 then {abort(); "";} else
      (let converter : A2I <- new A2I, 
      operand1 : Int <- converter.a2i(st.pop()),
      operand2 : Int <- converter.a2i(st.pop()),
      result : String <- converter.i2a(operand1 + operand2) in
        {
          st.push(result);
          result;
        }
      )
    fi
  };

  execute(st: Stack): SELF_TYPE {
    -- 栈为空，不进行操作
    if st.empty() then self else
    (let topStr_ : String <- st.top() in 
      {
        -- 栈顶是+号，pop掉+号后 相加栈顶的2个数字
        if topStr_ = "+" then { st.pop(); add_top(st);} else
        -- 栈顶是s，pop掉s后 交换栈顶的2个数字
        if topStr_ = "s" then { st.pop(); swap_top(st);} else
        -- 栈顶是整数, 无操作
        { self; } 
        fi fi;
        self;
      }
    )
    fi
  };

};

class DisplayCommand inherits Command {
  execute(st: Stack) : SELF_TYPE {
    {
      st.display();
      self;
    }
  };
};

class Main inherits IO {

  stack : Stack <- new Stack;

  create_command(s: String) : Command {
    -- add command
    if s="+" then new PushCommand.init(s) else
    -- push s command
    if s="s" then new PushCommand.init(s) else
    -- evalute
    if s="e" then new EvaluteCommand.init(s) else
    -- display
    if s="d" then new DisplayCommand.init(s) else
    -- int 
    new PushCommand.init(s)
    fi fi fi fi
  };

  main() : Object {
    -- enter 'x' to exit loop
    let input : String <- in_string(), cmd : Command in {
        while (not input = "x") loop
          {
            out_string(">".concat(input).concat("\n"));
            cmd <- create_command(input);
            cmd.execute(stack);
            input <- in_string();
          } 
        pool;
        input;
    }
  };
};
