# Chapter2 操作和表示信息

## 2.1

A. 0010 0101 1011 1001 1101 0010
B. 0xAE49
C. 1010 1000 1011 0011 1101
D. 0x322D96

## 2.2

n | $2^n$(decimal)  | $2^n$(hexadecimal)
-- | -- | --
23 | 8,388,608 | 0x800000
15 | 32,768 | 0x8000
14 | 16,384 | 0x2000
12 | 4,096 | 0x1000
6 | 64 | 0x40

## 2.3

Decimal  | Binary  | Hexadecimal
-- | -- | --
158 | 1001 1110 | 0x9E
76 | 0100 1100 | 0x4C
145 | 1001 0001 | 0x91
174 | 1010 1110 | 0xAE
60 | 0011 1100 | 0x3C
241 | 1111 0001 | 0xF1

## 2.4

A. 0x6062
B. 0x603c
C. 0x607c
D. 0x9e

## 2.5

A. L: 0x78  B: 0x12
B. L: 0x78 0x56   B: 0x12 0x34
C. L: 0x78 0x56 0x34   B: 0x12 0x34 0x56

## 2.6 <---notes--->

A. 
C. ?

## 2.7

0x68 0x69 0x70 0x71 0x72 0x73

## 2.9

A. White Yellow Magenta Red Cyan Green Blue Black. 互为相反颜色

B. 颜色或表示颜色混合，颜色与表示？

- Blue | Green = Cyan
- Yellow & Cyan = Green
- Red ^ Magenta = Blue

## 2.10

- | a | b
-- | -- | --
Step1 | a | a^b
Step2 | b | a^b
Step3 | b | a

## 2.11

A. first = last = k
B. a[k] ^ a[k] = 0
C. first <= last --> first < last

## 2.12

与0xFF来做掩码
异或0xFF用来求补
或0x00可以保持不变，0xFF全部置为1
为了可移植，需要求反来填充最高有效位

A. x & 0xFF
B. x ^ (~0xFF)
C. x | 0xFF

## 2.13 (note:异或的模拟推导思路)

符号化bis和bic之后,运用布尔代数进行推导

问题： {xor, or} 是一个完备集 adequate sets of connective 吗？ 或者可以用来定义 {not非}吗

bool_or : bis(x, y)
bool_xor: bis(bic(x,y), bic(~x, ~y))

**注意** bool_xor不应该出现非，有简单处理办法哦

## 2.15

!(x^y)