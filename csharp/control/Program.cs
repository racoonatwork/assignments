﻿using System;

namespace control
{
    class Program
    {
        /// <summary>
        /// 1. 计算圆的面积
        /// </summary>
        /// <param name="radius">圆的半径</param>
        /// <returns>圆的面积</returns>
        public static float AreaOfCircle(float radius)
        {
            return 3.14156F * radius * radius;
        }

        /// <summary>
        /// 2. 编写一个程序接受用户输入的字符。如果输入的字母是“a”、“e”、“i”、“o”或“u”中的一个，则显示“输入了一个元音”，否则显示“这不是一个元音”
        /// </summary>
        /// <param name="c">需要检查的字符</param>
        public static void CheckIsVowel(char c)
        {
            if (c == 'a' 
            || c == 'e'
            || c == 'i'
            || c == 'o'
            || c == 'u'
            )
            {
                Console.WriteLine("输入了一个元音");
            }
            else
            {
                Console.WriteLine("这不是一个元音");
            }
        }

        /// <summary>
        /// 3. 计算n!
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static ulong Factorial(ushort n )
        {
            ulong result = 1;
            for (ushort i = 1; i <= n; i++)
            {
                result *= i;
            }
            return result;
        }

        /// <summary>
        /// 4．编写程序，判断用户输入的一个正整数是否是素数
        /// </summary>
        /// <param name="n">需要判断的正整数</param>
        /// <returns>true是素数，false不是</returns>
        public static bool IsPrime(uint n)
        {
            return true;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
