#include "StaticLinkedList.h"

#include <iostream>
using std::size_t;
using std::vector;

template <typename T>
StaticLinkedList<T>::StaticLinkedList()
    : StaticLinkedList(0)
{
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::initializer_list<T> l)
    : StaticLinkedList(l.size())
{
    for (auto e : l)
    {
        push_back(e);
    }
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::size_t capacity)
{
    _list = std::vector<ListNode>(capacity + 2);
    // 所有的结点都没有被使用，0位置开始的ptr_index构成了一个没有使用的地址链表
    _list[UNUSED_NODE_HEAD_INDEX].ptr_index = begin() + 1;
    for (int i = _list[UNUSED_NODE_HEAD_INDEX].ptr_index; i < _list.size() - 1; ++i)
    {
        _list[i].ptr_index = i + 1;
    }
    // 最后一个结点指向开始
    _list[_list.size() - 1].ptr_index = UNUSED_NODE_HEAD_INDEX;

    // 指向自己的头结点
    _list[HEAD_NODE_INDEX].ptr_index = HEAD_NODE_INDEX;
}

template <typename T>
StaticLinkedList<T>::~StaticLinkedList()
{
}

template <typename T>
std::vector<T>
StaticLinkedList<T>::to_vector() const
{
    vector<T> result(size());
    // 空列表
    int i = begin();
    size_t count = 0;
    while (i != end())
    {
        const ListNode &node = _list[i];
        result[count++] = node.data;

        i = next(i);
    }

    return result;
}

template <typename T>
bool StaticLinkedList<T>::insert(const T &element, int pos)
{
    // 必须是有效的位置
    if (pos < 0 || pos > size())
        return false;
    // 找到需要插入位置的前一个元素
    int prev_index = index_of_nth_element(pos);

    // 分配一个新结点
    size_t index = allocated_index();
    _list[index].data = element;

    // 插入该结点到之前搜索的元素之后
    size_t next_index = _list[prev_index].ptr_index;
    _list[prev_index].ptr_index = index;
    _list[index].ptr_index = next_index;

    // 更新size
    _size += 1;

    return true;
}

template <typename T>
void StaticLinkedList<T>::push_back(const T &element)
{
    insert(element, size());
}

template <typename T>
bool StaticLinkedList<T>::remove(const T &element)
{
    // 找到元素，并记住前一个元素
    int prev_index = end();
    
    // 从第一个元素开始遍历
    int i = begin();
    while (i != end())
    {
        const ListNode &node = _list[i];
        if (node.data == element)
            break;
        prev_index = i;
        i = next(i);
    }

    // 没有找到
    if (i == end())
    {
        return false;
    }
    else
    {
        // 将之前的元素指向下一个元素
        _list[prev_index].ptr_index = _list[i].ptr_index;
        // 删除该结点，即回收该结点
        deallocate_with_index(i);

        _size -= 1;
        return true;
    }
}

template <typename T>
T &StaticLinkedList<T>::find(const T &element)
{
    // 从第一个元素开始遍历
    int i = begin();
    while (i != end())
    {
        const ListNode &node = _list[i];
        if (node.data == element)
            break;
        i = next(i);
    }
    return _list[i].data;
}

template <typename T>
size_t StaticLinkedList<T>::allocated_index()
{
    // 当前空闲结点
    size_t unused_index = _list[0].ptr_index;
    // 没有空闲结点了
    if (unused_index == 0)
    {
        // 添加新的结点
        ListNode newNode;
        _list.push_back(newNode);
        unused_index = _list.size() - 1;
    }
    else
    {
        // 指向下一个空闲结点
        _list[0].ptr_index = _list[unused_index].ptr_index;
    }
    return unused_index;
}

template <typename T>
void StaticLinkedList<T>::deallocate_with_index(std::size_t index)
{
    std::size_t tmp_index = _list[0].ptr_index;
    _list[0].ptr_index = index;
    _list[index].ptr_index = tmp_index;
}

template <typename T>
int StaticLinkedList<T>::index_of_nth_element(int n)
{
    if (n <= 0 || n > size())
        return end();
    // 找到第n-1个元素，然后返回其指向的索引即可
    int count = 0;
    int i = begin();
    while (i != end())
    {
        ++count;
        if (count == n)
            break;
        i = next(i);
    }
    return i;
}

using namespace std;

template<typename T>
void print_vector(const std::vector<T>& vec)
{
    cout << "vec = ";
    for (auto v : vec)
    {
        cout << v << ", ";
    }
    cout << endl;
}

int main()
{
    StaticLinkedList<int> li{1,2,3,5,6,3,5};
    li.remove(2);
    li.remove(5);
    li.insert(10,0);
    li.insert(15,3);
    li.insert(-1,100);
    li.insert(-3,3);

    StaticLinkedList<string> ls{"he", "ih", "world", "!"};
    ls.insert("abc", -1);
    ls.insert("efg", 1);
    std::string t = ls.find("!s");
    cout << t << endl;

    string s1("hello world!");
    string s2(s1);
    string& s3 = s1;
    string& s4 = s2;

    if (&s3 == &s4)
        cout << "equal" << endl;
    else
    {
        cout << "not equal" << endl;
    }
    

    print_vector(ls.to_vector());

    return 0;
}