#ifndef _STATIC_LINKED_LIST_H_
#define _STATIC_LINKED_LIST_H_

#include <vector>
#include <iterator>
#include <initializer_list>

template <typename T>
class StaticLinkedListIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;

    typedef StaticLinkedListIterator<T> self_type;

public:
    StaticLinkedListIterator(const std::vector<T> &vec, int index);

public:
    self_type &operator++()
    {
        _index = _vec[_index].ptr_index;
        return *this;
    }
    self_type &operator++(int)
    {
        self_type tmp = *this;
        _index = _vec[_index].ptr_index;
        return tmp;
    }

private:
    int _index;
    std::vector<T> _vec;
};

/**
 * 一个静态链表, 提供插入，删除和查找功能
 * @note T类型必须有默认构造函数
 */
template <typename T>
class StaticLinkedList
{
public:
    StaticLinkedList();
    StaticLinkedList(std::initializer_list<T> l);
    // capacity: vector的capacity，预估的大小
    explicit StaticLinkedList(std::size_t capacity);
    ~StaticLinkedList();

public:
    /**
     * @brief 返回元素的数目
     */
    std::size_t size() const { return _size; }

    // 是否为空
    bool empty() const { return size() == 0; }

    /**
     * @brief 返回一个包含所有数据的vector
     */
    std::vector<T> to_vector() const;

    /**
     * @brief 在指定的位置index插入一个元素element
     * 
     * @param element 需要插入的元素
     * @param pos 插入的位置，0表示第一个
     * @return true 插入成功
     * @return false 插入失败，当pos无效时
     */
    bool insert(const T &element, int pos);

    /**
     * @brief 在链表尾部插入一个元素
     * 
     * @param element 待插入的元素
     */
    void push_back(const T &element);

    /**
     * @brief 删除一个元素，只会删除找到的第一个元素
     * 
     * @param element 需要删除的元素 
     * @return true 成功删除该元素
     * @return false 没有该元素，删除失败
     */
    bool remove(const T &element);

    T &find(const T &element);

private:
    // 链表的头结点在数组中的索引
    static const int UNUSED_NODE_HEAD_INDEX = 0;
    // 链表的头结点/哨兵结点在数组中的索引
    static const int HEAD_NODE_INDEX = 1;
    // 第一个元素的位置
    std::size_t begin() const { return empty() ? HEAD_NODE_INDEX : _list[HEAD_NODE_INDEX].ptr_index; }
    // 最后一个元素之后的位置
    std::size_t end() const { return HEAD_NODE_INDEX; }
    // 下一个位置
    std::size_t next(int i) const { return _list[i].ptr_index; }

private:
    /**
     * @brief 内部使用的链表结点
     */
    typedef struct StaticLinkedListNode
    {
        // 结点内的数据
        T data;
        // 指向的结点索引
        std::size_t ptr_index;
    } ListNode;

private:
    // 分配一个空闲位置
    std::size_t allocated_index();

    // 回收指定位置的
    void deallocate_with_index(std::size_t index);

    // 找到第n个元素的索引， 返回end()表示没有找到
    int index_of_nth_element(int n);

private:
    std::vector<ListNode> _list;
    std::size_t _size = 0;
};

#endif