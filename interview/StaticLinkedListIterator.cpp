#include "StaticLinkedListIterator.h"

#include <iostream>
using std::size_t;
using std::vector;

template <typename T>
StaticLinkedList<T>::StaticLinkedList()
    : StaticLinkedList(0)
{
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::initializer_list<T> l)
    : StaticLinkedList(l.size())
{
    for (auto e : l)
    {
        push_back(e);
    }
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::size_t capacity)
{
    _list = std::vector<ListNode>(capacity + 2);
    // 指向自己的头结点/哨兵结点
    _list[HEAD_NODE_INDEX].ptr_index = HEAD_NODE_INDEX;
    // 所有的结点都没有被使用，0位置作为表头构成了一个没有使用的地址链表
    if (capacity == 0)
    {
        _list[UNUSED_NODE_HEAD_INDEX].ptr_index = UNUSED_NODE_HEAD_INDEX;
    }
    else
    {
        _list[UNUSED_NODE_HEAD_INDEX].ptr_index = HEAD_NODE_INDEX + 1;
        for (int i = HEAD_NODE_INDEX+1; i < _list.size() - 1; ++i)
        {
            _list[i].ptr_index = i + 1;
        }
        _list[_list.size()-1].ptr_index = UNUSED_NODE_HEAD_INDEX;
    }
}

template <typename T>
StaticLinkedList<T>::~StaticLinkedList()
{
}

template <typename T>
std::vector<T>
StaticLinkedList<T>::to_vector() const
{
    vector<T> result(size());
    std::copy(begin(), end(), result.begin());
    return result;
}

template <typename T>
bool StaticLinkedList<T>::insert(const T &element, int pos)
{
    // 必须是有效的位置
    if (pos < 0 || pos > size())
        return false;
    // 找到需要插入位置的前一个元素
    int prev_index = index_of_nth_element(pos);

    // 分配一个新结点
    size_t index = allocate_index();
    _list[index].data = element;

    // 插入该结点到之前搜索的元素之后
    size_t next_index = _list[prev_index].ptr_index;
    _list[prev_index].ptr_index = index;
    _list[index].ptr_index = next_index;

    // 更新size
    _size += 1;

    return true;
}

template <typename T>
void StaticLinkedList<T>::push_back(const T &element)
{
    insert(element, size());
}

template <typename T>
bool StaticLinkedList<T>::remove(const T &element)
{
    // 找到元素，并记住前一个元素
    auto prev_index = end();
    
    // 从第一个元素开始遍历
    auto iter = begin();
    for (; iter != end(); ++iter)
    {
        if (*iter == element)
            break;
        prev_index = iter;
    }

    // 没有找到
    if (iter == end())
    {
        return false;
    }
    else
    {
        // 将之前的元素指向下一个元素
        _list[prev_index._index].ptr_index = _list[iter._index].ptr_index;
        // 删除该结点，即回收该结点
        deallocate_with_index(iter._index);

        _size -= 1;
        return true;
    }
}

template <typename T>
typename StaticLinkedList<T>::iterator StaticLinkedList<T>::find(const T &element)
{
    // 从第一个元素开始遍历
    iterator iter = begin();
    for (; iter != end(); ++iter)
    {
        if (*iter == element)
            return iter;
    }
    return iter;
}

template <typename T>
typename StaticLinkedList<T>::const_iterator StaticLinkedList<T>::find(const T&element) const
{
    // 从第一个元素开始遍历
    const_iterator iter = cbegin();
    for (; iter != cend(); ++iter)
    {
        if (*iter == element)
            return iter;
    }
    return iter;
}

template <typename T>
size_t StaticLinkedList<T>::allocate_index()
{
    // 当前空闲结点
    size_t unused_index = _list[0].ptr_index;
    // 没有空闲结点了
    if (unused_index == 0)
    {
        // 添加新的结点
        ListNode newNode;
        _list.push_back(newNode);
        unused_index = _list.size() - 1;
    }
    else
    {
        // 指向下一个空闲结点
        _list[0].ptr_index = _list[unused_index].ptr_index;
    }
    return unused_index;
}

template <typename T>
void StaticLinkedList<T>::deallocate_with_index(std::size_t index)
{
    std::size_t tmp_index = _list[0].ptr_index;
    _list[0].ptr_index = index;
    _list[index].ptr_index = tmp_index;
}

template <typename T>
int StaticLinkedList<T>::index_of_nth_element(int n)
{
    if (n <= 0 || n > size())
        return end()._index;
    // 找到第n-1个元素，然后返回其指向的索引即可
    int count = 0;
    auto iter = begin();
    for (; iter != end(); ++iter)
    {
        ++count;
        if (count == n)
            break;
    }
    return iter._index;
}

// --------------------------
// 简单的测试

#include <iostream>
#include <algorithm>
#include <list>
using namespace std;

void test_find()
{
    StaticLinkedList<int> li{3,1,5,6,7,8,10, -1,-5,1};
    auto iter = li.find(1);

    auto iter2 = find(li.begin(), li.end(), 1);

    if (iter == iter2)
    {
        cout << "find and std::find works" << endl;
    }
    else
    {
        cout << "iter1 = " << *iter << "iter2 = " << *iter2 << endl;
    }
}

void test_to_vector()
{
    StaticLinkedList<string> ls{"How", "are", "you", "?"};
    auto v = ls.to_vector();
    copy(v.begin(), v.end(), ostream_iterator<string>(cout, ","));
    cout << endl;
}

void test_algorithm()
{
    StaticLinkedList<int> li{3,1,5,6,7,8,10,-1,-5,1};
    auto min_iter = std::min_element(li.begin(), li.end());
    cout << "min " <<  *min_iter << endl;

    std::copy(li.begin(), li.end(), ostream_iterator<int>(cout, ","));
    cout << endl;
}

template<typename T>
void print_vector(const vector<T>& vec)
{
    std::copy(vec.begin(),
    vec.end(),
    ostream_iterator<T>(cout, ", "));
    cout << endl;
}

template<typename T>
void print_list(const StaticLinkedList<T>& l)
{
    std::copy(l.begin(),
    l.end(),
    ostream_iterator<T>(cout, ", "));
    cout << endl;
}

void test_remove()
{
    cout << "test remove" << endl;
    StaticLinkedList<int> li{1,2,4,5,-1,-4,5};
    std::remove(li.begin(), li.end(), 2);
    print_list(li);

    std::remove(li.begin(), li.end(), 5);
    print_list(li);
    cout << li.size();
    cout << li.end()._index;
    cout << endl;
}

int main()
{
    test_find();

    test_algorithm();

    test_to_vector();
    
    test_remove();

    StaticLinkedList<int> li;
    li.remove(2);
    li.remove(5);
    li.insert(10,0);
    li.insert(15,3);
    li.insert(-1,100);
    li.insert(-3,3);
    print_vector(li.to_vector());

    StaticLinkedList<string> ls{"he", "ih", "world", "!"};
    ls.insert("abc", -1);
    ls.insert("efg", 1);
    StaticLinkedList<string>::iterator iter = ls.find("!s");
    if (iter == ls.end())
        cout << "not found!" << endl;
    else
    {
        cout << "find iter" << endl;
    }

    print_vector(ls.to_vector());
    
    StaticLinkedList<float> lf(10);
    lf.insert(5.01f, 0);
    lf.push_back(3.0f);
    lf.remove(4.4f);

    print_vector(lf.to_vector());

    return 0;
}