#ifndef __STATIC_LINKED_LIST_H__
#define __STATIC_LINKED_LIST_H__

#include <vector>
#include <iterator>
#include <initializer_list>

/**
 * @brief 静态链表结点
 */
template <typename T>
struct StaticLinkedListNode
{
    // 结点内的数据
    T data;
    // 指向的结点索引
    std::size_t ptr_index;
};

template <typename T>
class StaticLinkedListIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;

    typedef StaticLinkedListIterator<T> self_type;
    typedef std::vector<StaticLinkedListNode<T>> node_vector;

public:
    StaticLinkedListIterator(node_vector &vec, size_t index) : _vec(vec), _index(index) {}
    StaticLinkedListIterator &operator=(const StaticLinkedListIterator &other)
    {
        _vec = other._vec;
        _index = other._index;
        return *this;
    }
    StaticLinkedListIterator(const StaticLinkedListIterator &other)
        : StaticLinkedListIterator(other._vec, other._index) {}

public:
    reference operator*() const
    {
        return _vec[_index].data;
    }

    pointer operator->() const
    {
        return &_vec[_index].data;
    }

    self_type &operator++()
    {
        _index = _vec[_index].ptr_index;
        return *this;
    }
    self_type &operator++(int)
    {
        self_type tmp = *this;
        _index = _vec[_index].ptr_index;
        return tmp;
    }

    bool operator==(const self_type &other) const
    {
        return _index == other._index && (&_vec == &other._vec);
    }

    bool operator!=(const self_type &other) const
    {
        return _index != other._index || (&_vec != &other._vec);
    }

public:
    size_t _index;
    node_vector &_vec;
};

template<typename T>
bool operator == (const StaticLinkedListIterator<T>& iter1, const StaticLinkedListIterator<T>& iter2)
{
    return iter1._index == iter2._index && (&iter1._vec == &iter2._vec);
}

template<typename T>
bool operator != (const StaticLinkedListIterator<T>& iter1, const StaticLinkedListIterator<T>& iter2)
{
    return iter1._index != iter2._index || (&iter1._vec != &iter2._vec);
}


template <typename T>
class StaticLinkedListConstIterator
{
public:
    using iterator_category = std::forward_iterator_tag;
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = const T *;
    using reference = const T &;

    typedef StaticLinkedListConstIterator<T> self_type;
    typedef std::vector<StaticLinkedListNode<T>> node_vector;

public:
    StaticLinkedListConstIterator(const node_vector &vec, size_t index) : _vec(vec), _index(index) {}

    StaticLinkedListConstIterator &operator=(const StaticLinkedListConstIterator &other)
    {
        _vec = other._vec;
        _index = other._index;
        return *this;
    }
    StaticLinkedListConstIterator(const StaticLinkedListConstIterator &other)
        : StaticLinkedListConstIterator(other._vec, other._index) {}
public:
    reference operator*() const
    {
        return _vec[_index].data;
    }

    pointer operator->() const
    {
        return &_vec[_index].data;
    }

    self_type &operator++()
    {
        _index = _vec[_index].ptr_index;
        return *this;
    }
    self_type &operator++(int)
    {
        self_type tmp = *this;
        _index = _vec[_index].ptr_index;
        return tmp;
    }

    bool operator==(const self_type &other) const
    {
        return _index == other._index && (&_vec == &other._vec);
    }

    bool operator!=(const self_type &other) const
    {
        return _index != other._index || (&_vec != &other._vec);
    }

public:
    size_t _index;
    const node_vector &_vec;
};

template<typename T>
bool operator == (const StaticLinkedListConstIterator<T>& iter1, const StaticLinkedListConstIterator<T>& iter2)
{
    return iter1._index == iter2._index && (&iter1._vec == &iter2._vec);
}

template<typename T>
bool operator != (const StaticLinkedListConstIterator<T>& iter1, const StaticLinkedListConstIterator<T>& iter2)
{
    return iter1._index != iter2._index || (&iter1._vec != &iter2._vec);
}

/**
 * 一个静态链表, 提供插入，删除和查找功能
 * @note T类型必须有默认构造函数
 * @TODO std::remove等方法会导致_size大小不正确
 */
template <typename T>
class StaticLinkedList
{
public:
    StaticLinkedList();
    StaticLinkedList(std::initializer_list<T> l);
    // capacity: 容量capacity，预估的大小
    explicit StaticLinkedList(std::size_t capacity);
    ~StaticLinkedList();

public:
    typedef StaticLinkedListConstIterator<T> const_iterator;
    typedef StaticLinkedListIterator<T> iterator;

    // 链表的头结点在数组中的索引
    static const size_t UNUSED_NODE_HEAD_INDEX = 0;
    // 链表的头结点/哨兵结点在数组中的索引
    static const size_t HEAD_NODE_INDEX = 1;

    iterator begin()
    {
        return iterator(_list,
                        empty() ? HEAD_NODE_INDEX : _list[HEAD_NODE_INDEX].ptr_index);
    }

    const_iterator begin() const
    {
        return const_iterator(_list,
                              empty() ? HEAD_NODE_INDEX : _list[HEAD_NODE_INDEX].ptr_index);
    }

    iterator end() { return iterator(_list, HEAD_NODE_INDEX); }

    const_iterator end() const { return const_iterator(_list, HEAD_NODE_INDEX); }

    const_iterator cbegin() const
    {
        return const_iterator(_list,
                              empty() ? HEAD_NODE_INDEX : _list[HEAD_NODE_INDEX].ptr_index);
    }

    const_iterator cend() const { return const_iterator(_list, HEAD_NODE_INDEX); }

public:
    /**
     * @brief 返回元素的数目
     */
    std::size_t size() const { return _size; }

    // 是否为空
    bool empty() const { return size() == 0; }

    /**
     * @brief 返回一个包含所有数据的vector
     */
    std::vector<T> to_vector() const;

    /**
     * @brief 在指定的位置index插入一个元素element
     * 
     * @param element 需要插入的元素
     * @param pos 插入的位置，0表示第一个
     * @return true 插入成功
     * @return false 插入失败，当pos无效时
     */
    bool insert(const T &element, int pos);

    /**
     * @brief 在链表尾部插入一个元素
     * 
     * @param element 待插入的元素
     */
    void push_back(const T &element);

    /**
     * @brief 删除一个元素，只会删除找到的第一个元素
     * 
     * @param element 需要删除的元素 
     * @return true 成功删除该元素
     * @return false 没有该元素，删除失败
     */
    bool remove(const T &element);

    /**
     * @brief 查找元素
     * 
     * @param element 需要查找的元素
     * @return iterator 返回对应位置的迭代器，如果没有找到返回end()
     */
    iterator find(const T &element);
    const_iterator find(const T &element) const;

private:
    // // 第一个元素的位置
    // std::size_t begin() const { return empty() ? HEAD_NODE_INDEX : _list[HEAD_NODE_INDEX].ptr_index; }
    // // 最后一个元素之后的位置
    // std::size_t end() const { return HEAD_NODE_INDEX; }
    // // 下一个位置
    // std::size_t next(int i) const { return _list[i].ptr_index; }

private:
    // 分配一个空闲位置
    std::size_t allocate_index();

    // 回收指定位置的
    void deallocate_with_index(std::size_t index);

    // 找到第n个元素的索引， 返回end()表示没有找到
    int index_of_nth_element(int n);

private:
    typedef StaticLinkedListNode<T> ListNode;
    std::vector<ListNode> _list;
    std::size_t _size = 0;
};

#endif