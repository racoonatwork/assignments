// 简单的单元测试文件
#include "StaticLinkedList.h"
#include "gtest/gtest.h"

using namespace std;

TEST(SLLTest, test_create)
{
    StaticLinkedList<int> list1{1,2,3,5};
    StaticLinkedList<string> l2{"hello", "world", "!"};

    EXPECT_EQ(list1.to_vector(), vector<int>({1,2,3,5}));
}


void test_search()
{

}

void test_delete()
{
    StaticLinkedList<int> list2{1,2,3,5,6};
    list2.remove(2);
}

void test_insert_list()
{

}

int main(int argc, char **argv)
{
    // ::testing::InitGoogleTest(&argc, argv);
    // return RUN_ALL_TESTS();
    test_delete();
    return 0;
}