#include "StaticLinkedList.h"
#include <vector>

using std::vector;
using std::size_t;

template <typename T>
StaticLinkedList<T>::StaticLinkedList()
    : StaticLinkedList(0)
{
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::initializer_list<T> l)
    : StaticLinkedList(l.size())
{
    for (auto e: l)
    {
        push_back(e);
    }
}

template <typename T>
StaticLinkedList<T>::StaticLinkedList(std::size_t capacity)
{
    _list = std::vector<T>(capacity + 1);
    // 所有的结点都没有被使用，0位置开始的ptr_index表示了一个没有使用的列表地址的链表
    for (int i = 0; i < _list.size() - 1; ++i)
    {
        _list[i].ptr_index = i+1;
    }
    // 最后一个结点指向开始
    _list[_list.size() - 1].ptr_index = 0;
}

template <typename T>
StaticLinkedList<T>::~StaticLinkedList()
{
}

template <typename T>
std::vector<T>
StaticLinkedList<T>::to_vector() const
{
    vector<T> result(size());
    // 空列表
    if (empty())
    {
        return result;
    }
    
    // 从第一个元素开始遍历
    const ListNode& node = _list[1];
    while (node.ptr_index != 0)
    {
        result.push_back(node);
        node = _list[node.ptr_index];
    }
    
    return result;
}

template <typename T>
bool StaticLinkedList<T>::insert(const T &element, int pos)
{
    // 必须是有效的位置
    if (pos < 0 || pos >= size())
        return false;
    // 找到需要插入位置的前一个元素
    int prev_index = index_of_nth_element(pos);
    
    // 分配一个新结点
    size_t index = allocated_index();
    _list[index].data = element;

    // 插入该结点到之前搜索的元素之后
    size_t next_index = _list[prev_index].ptr_index;
    _list[prev_index].ptr_index = index;
    _list[index].ptr_index = next_index;

    // 更新size
    _size += 1;

    return true;
}

template <typename T>
std::size_t StaticLinkedList<T>::push_back(const T& element)
{
    insert(element, size());
}

template <typename T>
bool StaticLinkedList<T>::remove(const T& element)
{
}

template <typename T>
T& StaticLinkedList<T>::find(const T& element)
{

}

template <typename T>
std::size_t StaticLinkedList<T>::allocated_index()
{
    // 当前空闲结点
    unused_index = _list[0].ptr_index;
    // 没有空闲结点了
    if (unused_index == 0)
    {
        // 添加新的结点
        ListNode newNode;
        newNode.ptr_index = 0;
        _list.push_back(newNode);
        unused_index = _list.size() - 1;
    }
    else 
    {
    // 指向下一个空闲结点
        _list[0].ptr_index = _list[unused_index].ptr_index;
    }
    return unused_index;
}

template <typename T>
int StaticLinkedList<T>::index_of_nth_element(int n)
{
    if (n <= 0 || n > this->size())
        return -1;
    // 第一个元素就是索引为1的位置
    if (n == 1)
        return 1; 
    // 找到第n-1个元素，然后返回其指向的索引即可
    int count = 0;
    ListNode& node = _list[1];
    while (count < n)
    {
        node = _list[node.ptr_index];
        ++count;
    }
    return node.ptr_index;
}