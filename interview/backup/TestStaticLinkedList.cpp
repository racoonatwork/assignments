// 简单的单元测试文件
#include "StaticLinkedList.h"
#include <vector>
#include <string>
#include <assert.h>
#include <sstream>
#include <iterator>
#include <iostream>
#include <list>

using namespace std;

void setup()
{

}

template<typename T>
string vector_to_string(const std::vector<T>& vec)
{
    stringstream ss;
    std::copy(vec.begin(), vec.end(), ostream_iterator<char>(ss, ","));
    // bool first = true;
    // for (auto v: vec)
    // {
    //     if (!first)
    //         ss << ", "
    //     ss << v;
    //     first = false;
    // }
    return ss.str().c_str();
}


template<typename T>
void assert_array_equal(const std::vector<T>& arr1, const std::vector<T>& arr2)
{
    if (arr1 != arr2)
    {
        string message = "arr1:" + vector_to_string<T>(arr1);
        message += "\n";
        message += "arr2: " + vector_to_string<T>(arr1);
        std::cerr << message;
        // assert(false);
    }
    
}

void test_create()
{
}

void test_search()
{

}

void test_delete()
{

}

void test_insert_list()
{

}

int main()
{
    //简单的测试
    test_search();
    test_delete();
    test_insert_list();

    assert_array_equal<int>({1,2,3}, {2,3,5});

    std::list<int> li;
}