local M = {}

--- Exercise 21.1 Implement a class Stack
-- 默认的堆栈大小
local DEFAULT_STACK_SIZE = 10

local Stack = {
  arr_ = {},
  max_size_ = DEFAULT_STACK_SIZE,
}

function Stack:new(s, size)
  s = s or {}
  size = size or DEFAULT_STACK_SIZE
  self.__index = self
  self.max_size_ = size
  setmetatable(s, self)
  return s
end

function Stack:max_size()
  return self.max_size_ or DEFAULT_STACK_SIZE
end

function Stack:push(v)
  if #self.arr_ == self:max_size() then
    print("stack overflow")
  else
    self.arr_[#self.arr_ + 1] = v
  end
end

function Stack:pop()
  if self:isempty() then
    print("pop empty stack")
  else
    local top_value = self:top()
    self.arr_[#self.arr_] = nil
    return top_value
  end
end

function Stack:top()
  return self.arr_[#self.arr_]
end

function Stack:isempty()
  return #self.arr_ == 0
end

function M.e_21_1()
  local s = Stack:new({}, 2)
  -- local s = Stack:new {}
  s:push(3)
  s:push("number 4")
  print("s.size = " .. s:max_size())
  s:push("stackoverflow")
  print(s:pop())
  print(s:isempty())
  print(s:top())
  print(s:pop())
  print(s:isempty())
  print(s:pop())
end

return M
