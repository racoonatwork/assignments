local config = {
  [21] = {
    total_num = 4,
    name = "Object-Oriented Programming"
  }
}

test_ci = {123, 234}

local function get_chapter_name(chapter)
  local prefix = "Chapter" .. chapter
  local name = (config[chapter] or {}).name
  if name ~= nil and name ~= "" then
    return prefix .. ":" .. name
  else
    return prefix
  end
end

local function get_total_num(chapter)
  return config[chapter].total_num or 0
end

local function get_chapters()
  local chapters = {}
  for k in pairs(config) do
    chapters[#chapters + 1] = k
  end
  return chapters
end

return {
  get_chapter_name = get_chapter_name,
  get_total_num = get_total_num,
  get_chapters = get_chapters
}
