-- 给书中的练习提供一些常用的函数，方便输出答案

local M = {}

local config = require("config")

function M.exercise_name(chapter, num)
  return "exercise " .. chapter .. "." .. num
end

function M.get_answer_func(chapter, num)
  local m = require("chapter"..chapter)
  local func_name = "e_" .. chapter .. "_" .. num
  return m[func_name]
end

function M.split_chapter_and_num(chapter_num)
  local chapter, num = math.modf(chapter_num)
  local num_string = tostring(num):sub(3, -1)
  num = tonumber(num_string)
  return chapter, num
end

function M.run_answer_func(chapter, num)
  local func = M.get_answer_func(chapter, num)
  local ename = M.exercise_name(chapter, num)
  if func ~= nil then
    print("Answer for " .. ename .. " :")
    func()
  else
    print("No Answer for " .. ename)
  end
  return func
end

function M.get_answer(chapter_num)
  local chapter, num = M.split_chapter_and_num(chapter_num)
  M.run_answer_func(chapter, num)
end

function M.get_answers(chapter)
  print("=====" .. config.get_chapter_name(chapter) .. "====")
  local count = 0
  local total_num = config.get_total_num(chapter)
  for i = 1, total_num do
    local func = M.run_answer_func(chapter, i)
    if func then
      count = count + 1
    end
  end
  if count == total_num then
    print("Well done!!!")
  else
    local progress_bar = "[" .. string.rep("=", count) .. string.rep(" ", total_num - count) .. "]"
    print("Progress " .. count .. "/" .. total_num .. progress_bar)
  end
end

return {
  get_answer = M.get_answer,
  get_answers = M.get_answers
}
