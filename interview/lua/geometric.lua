--[[A simple system for geometric regions
A geometric region is a set of points, 所以我们可以用其特征方程表示该集合区域
集合A的特征方程概念：
if x \in A, 则$f_A(x) = true$，否则为false
--]]

-- disk
function disk(cx, cy, r)
  return nil
end

-- axis-aligned rectangles
local function rect(left, right, bottom, up)
  return nil
end

-- complement
local function complement(r)
  return nil
end

local function translate(r, dx, dy)
  return function(x, y)
    return r(x - dx, y - dy)
  end
end

local function plot(r, M, N)
  io.write("P1\n", M, " ", N, "\n")  --header
  for i = 1, N do
    local y = (N - i * 2) / N  -- 1: +1-epsilon  N:-1
    for j = 1, M do
      local x = (2*j - M) / M  -- 1: -1+epsilon  M:+1
      io.write(r(x,y) and "1" or "0")
    end
    io.write("\n")
  end
end

return {
  plot = plot,
  disk = disk,
  rect = rect,
  translate = translate,
  complement = complement,
}