-- require("mystrict")
local config = require("config")
local exercise = require("exercise")

local function run_exercise()
  local chapters = config.get_chapters()
  for _, chapter in ipairs(chapters) do
    exercise.get_answers(chapter)
  end
end

run_exercise()
