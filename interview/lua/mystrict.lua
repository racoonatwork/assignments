--[[
只允许在main chunk或者 C语言中声明全局变量，
在函数等其它chunk中对未声明的全局变量赋值将会抛出错误
--]]

-- 声明过的全局变量, 允许将声明过的全局变量设置为nil
local declared = {}
-- 将_G中的key都放入declared中
for k in pairs(_G) do
  declared[k] = true
end

local mt = getmetatable(_G)
if mt == nil then
  mt = {}
  setmetatable(_G, mt)
end

mt.__index = function(_, n)
    -- 不能访问未声明的全局变量
    if not declared[n] then
      error("attempt to read undeclared global variable "..n, 2)
    else
      return nil
    end
  end

mt.__newindex = function(t, n, v)
    -- 检查该全局变量是否声明过（赋值被置为nil的全局变量会再次调用该函数)
    if not declared[n] then
      -- 如果不在main chunk或者C中，抛出错误
      local w = debug.getinfo(2, "S").what
      if w ~= "main" or w ~= "C" then
        error("attempt to assign undeclared global variable "..n, 2)
      end
    else
      declared[n] = true
    end
    -- 赋值
    rawset(t, n, v)
  end