# Chapter 1

## $1.3^{[3, p.16]}$

$(a)$ Show that the probability of error of $R_N$, the repetition code with $N$ repetitions, is $p_b = ...$ for odd $N$.

答: 根据`probability of bit error`的定义，$p_b = P(\hat s \neq s)$, 其中$s$是source bits, $\hat s$是decoding bits. 在repetition code中，$s, \hat s$都为1个bit，两者不相等的情况出现在传输的编码$t(s)$中，有超过一半的bits 反转了，也就是:

$$
p_b = \sum_{n=(N+1)/2}^N {N \choose n} f^n (1-f)^{N-n}
$$

$(b)$ Assuming $f = 0.1$, which of the terms in this sum is the biggest? How much bigger is it than the second-biggest term?

答: 当$n=(N+1)/2$ 时最大，比第二大的$n=(N+3)/2$ 约大$(1-f) / f = 9$倍， 确切值为$ {N+3 \over N-1} {1-f \over f}$

$(c)$ Use Stirling’s approximation $(p.2)$ to approximate the $N$ in the largest term and find, approximately, the probability of error of the repetition code with $N$ repetitions.

答: 根据Poission分布和正态分布在某些情况($\lambda 较大，P(r=\lambda|\lambda) \simeq N(r=\lambda|\lambda, \lambda)$)下的接近，我们可以得到Stirling’s approximation的更精确一点的近似$(1.12)$，从而有
$${N \choose N/2} \simeq  2^N \frac{1}{\sqrt{2 \pi N/4}}$$

$(d)$ Assuming $f = 0.1$, find how many repetitions are required to get
the probability of error down to $10^{−15}$.

## $1.6^{[2, p.17]}$

$(a)$ Calculate the probability of block error $p_B$ of the $(7, 4)$ Hamming code as a function of the noise level $f$ and show that to leading order it goes as $21f^2$.

$(b)^{[3]}$ Show that to leading order the probability of bit error $p_b$ goes as $9f^2$.
